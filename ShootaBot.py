def do_turn(pw):
    defend_allied_planets(pw)
    if len(pw.my_fleets()) >= 1:
        return

    if len(pw.my_planets()) == 0:
        return

    if len(pw.neutral_planets()) >= 1:
            dest = pw.neutral_planets()[0]
    else:
        if len(pw.enemy_planets()) >= 1:
            dest = pw.enemy_planets()[0]

    source = pw.my_planets()[0]
        
    num_ships = source.num_ships() / 2
    pw.debug('Num Ships: ' + str(num_ships))

    pw.issue_order(source, dest, num_ships)

# Sends reinforcments to allied planets that are under attack
def defend_allied_planets(pw):
    if len(pw.enemy_fleets()):
        return
    for enemy_fleet in pw.enemy_fleets():
        pw.debug("def")
        if enemy_fleet.destination_planet().owner() == 1:
            planet_under_attack = enemy_fleet.destination_planet()
            if enemy_fleet.num_ships > planet_under_attack.num_ships() + planet_under_attack.growth_rate() * enemy_fleet.turns_remaining():
                # Planet needs reinforcments
                if len(pw.my_planets) > 1:
                    for planet in pw.my_planets():
                        if planet != planet_under_attack:
                            if planet.num_ships >= enemy_fleet.num_ships() - planet_under_attack.num_ships() + planet_under_attack.growth_rate() * enemy_fleet.turns_remaining() + 1:
                                # Send reinforcments
                                pw.issue_order(planet, planet, enemy_fleet.num_ships() - planet_under_attack.num_ships() + planet_under_attack.growth_rate() * enemy_fleet.turns_remaining() + 1)
